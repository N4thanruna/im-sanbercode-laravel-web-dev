Jawaban.txt

1.Creating Database
CREATE DATABASE Myshop;

2.Making Table
Table Users
CREATE TABLE Users(
    id int PRIMARY KEY AUTO_INCREMENT,
    name varchar(255),
    email varchar(255),
    password varchar(255)
    )

Table Categories
CREATE TABLE Categories(
    id int PRIMARY KEY AUTO_INCREMENT,
    name varchar(255)
    )

Tabel Items
CREATE TABLE Items(
    id int PRIMARY KEY AUTO_INCREMENT,
    name varchar(255),
    description varchar(255),
    price int NOT null,
    stock int NOT null,
    category_id int NOT null,
    FOREIGN KEY(category_id) REFERENCES categories(id)
    );

3.Insert Data to Table
=====Inserting Users Data=====
INSERT INTO users(id, name, email, PASSWORD) VALUES(1, "John Doe", "john@doe.com","john123"),
(2, "Jane Doe", "jane@doe.com", "jenita123");

=====Inserting Categories Data=====
INSERT INTO categories(name) VALUES("gadget"), ("cloth"), ("men"), ("women"), ("branded");

=====Inserting Items Data======
INSERT INTO items(name, description, price, stock, category_id) VALUES("Sumsang b50", "hape keren dari merek sumsang", 4000000,	100,1), ("Uniklooh", "baju keren dari brand ternama", 	500000, 50, 2), ("IMHO Watch",	"jam tangan anak yang jujur banget", 2000000, 10, 1);

4.Mengambil Data dari Database
a. Mengambil data users
SELECT name,email FROM users;

b. Mengambil data items
-SELECT name, description, price, stock, category_id FROM items WHERE price > 1000000;
-SELECT name, description, price, stock, category_id FROM items WHERE name LIKE '%uniklo%';

c. Menampilkan data items join dengan kategori
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name 
FROM items 
INNER JOIN categories ON items.category_id=categories.id;

5.Mengubah Data dari Database
UPDATE items
SET price = 2500000
WHERE name = "Sumsang b50";

