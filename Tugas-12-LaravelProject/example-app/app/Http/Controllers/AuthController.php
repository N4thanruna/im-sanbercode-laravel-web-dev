<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis()
    {
        return view('halaman.register');
    }

    public function kirim(Request $request)
    {
        $namaDepan = $request['Fname'];
        $namaBelakang = $request['Lname'];

        return view('halaman.welcome', ['namadepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
