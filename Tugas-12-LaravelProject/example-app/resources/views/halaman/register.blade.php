<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="post">
        <br>
        <h3>Sign Up Form</h3>
        <br>
        <label>First name:</label>
        <input type="text" name="Firstnama"> <br>
        <label>Last name :</label>
        <input type="text" name="Lastnama"> <br>
        <br>
        <label>Gender:</label>
        <br>
        <ul>
            <input type="radio" name="Gender">Male <br>
            <input type="radio" name="Gender">Female <br>
            <input type="radio" name="Gender">Other <br>
        </ul>
        <br>
        <label>Nationality:</label>
        <br><br>
        <select>
            <option value="Indonesia">Indonesian</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Singaporean">Singaporean</option>
        </select><br> <br>
        <label>Languange Spoken:</label>
        <ul>
            <input type="checkbox">Bahasa Indonesia<br>
            <input type="checkbox">English<br>
            <input type="checkbox">Other<br>
        </ul>
        <label>Bio:</label>
        <br>
        <textarea name="Description" rows="10" cols="50"></textarea>
        <br>
    
            <input type="submit" value="Sign Up"/>

    </form>

</body>
</html>