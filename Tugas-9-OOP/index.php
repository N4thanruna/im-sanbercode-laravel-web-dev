<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Belajar OOP PHP</title>
</head>
<body>
    <?php
        require_once('animal.php');
        require_once('frog.php');
        require_once('ape.php');

        $sheep = new Animal("Shaun");  

        echo "Name: " . $sheep ->name . "<br>";
        echo "Legs: " . $sheep ->legs . "<br>";
        echo "Cold Blooded: " . $sheep ->cold_blooded . "<br>";

        echo "<br>";

        $toad = new Frog("buduk");
        echo "Name: " . $toad ->name . "<br>";
        echo "Legs: " . $toad ->legs . "<br>";
        echo "Cold Blooded: " . $toad ->cold_blooded . "<br>";
        echo "Yell: ";
        echo $toad ->YellFrog();
                
        echo "<br> <br>";

        $monkey = new Ape("kera sakti");
        echo "Name: " . $monkey ->name . "<br>";
        echo "Legs: " . $monkey ->legs . "<br>";
        echo "Cold Blooded: " . $monkey ->cold_blooded . "<br>";
        echo "Yell: ";
        echo $monkey ->YellApe();
        ?>
    
</body>
</html>